package BANK;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;

import BANK.sharedkernel.constant.Profiles;

/**
 * This class is used in WAR packaging to run eSpring boot application.
 *
 * 
 */
public class ApplicationWebXml
   extends SpringBootServletInitializer {

   private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationWebXml.class);

   @Override
   protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
      return application.profiles(addDefaultProfile())
         .sources(Application.class);
   }

   private String[] addDefaultProfile() {
      String profile = System.getProperty("spring.profiles.active");
      return new String[]{profile};
   }
}

package BANK.config.development;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.employee.finder.IEmployeeSnapshotFinder;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class DataInitializer {

   @Autowired
   private IEmployeeInitializer employeeInitializer;

   @Autowired
   private IEmployeeSnapshotFinder employeeSnapshotFinder;

   @Transactional
   @PostConstruct
   public void init() {
      if (employeeSnapshotFinder.findAll().isEmpty()) {
         employeeInitializer.initalizer();
      }
   }

}

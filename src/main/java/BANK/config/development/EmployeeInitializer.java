package BANK.config.development;

import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import BANK.domain.employee.bo.IEmployeeBO;
import BANK.sharedkernel.constant.Profiles;

@Component
@Profile(Profiles.DEVELOPMENT)
public class EmployeeInitializer
   implements IEmployeeInitializer {

  @Autowired
   private IEmployeeBO employeeBO;

   @Transactional
   @Override
   public void initalizer() {

      employeeBO.add(UUID.randomUUID(), "Mateusz", "Głąbicki",
         "Admin", "mateusz.glabicki",
         LocalDateTime.now(), "mateusz.glabicki@nsa.pl","ROLE_ADMIN","password");
      
      employeeBO.add(UUID.randomUUID(), "Mateusz", "Łędzewicz",
         "User", "mateusz.ledzewicz",
         LocalDateTime.now(), "mateusz.łędzewicz@nsa.pl","ROLE_USER","password");
      
      employeeBO.add(UUID.randomUUID(), "Mariusz", "Kłysiński",
         "User", "mariusz.klysinski",
         LocalDateTime.now(), "mariusz.klysinski@nsa.pl","ROLE_USER","password");
      
   }

}

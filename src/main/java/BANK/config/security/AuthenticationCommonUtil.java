package BANK.config.security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.security.web.context.SecurityContextPersistenceFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import BANK.web.filter.AuthenticatedUserLogFilter;

/**
 * This class is common Spring Security configuration. It allows for non secured access to static resources, login page,
 * REST API (endpoints are secured independent with @PreAuthorize annotation)
 *
 * 
 */
final class AuthenticationCommonUtil {

   private AuthenticationCommonUtil() {
   }

   final static void configure(WebSecurity web) {
      web.ignoring()
         .antMatchers(
            "/css/**",
            "/img/**",
            "/js/**",
            "/webjars/**",
            "/fonts/**");
   }

   final static void configure(HttpSecurity http,CustomLogoutHandler hander,AuthenticationSuccessHandler successHandler)
      throws Exception {
      http.authorizeRequests()
         .antMatchers("/login").permitAll()
         .antMatchers("/password").permitAll()
         .antMatchers("/sessionExpired").permitAll()
         .antMatchers("/api/**").permitAll()
         .anyRequest()
         .authenticated();

      http.formLogin()
         .loginPage("/login")
         .usernameParameter("username")
         .passwordParameter("password")
         .defaultSuccessUrl("/")
         .successHandler(successHandler)
         .failureUrl("/login?error");

      http.logout()
         .addLogoutHandler(hander)
         .logoutRequestMatcher(new AntPathRequestMatcher("/logout"));

      http.csrf().disable();

      http.addFilterAfter(new AuthenticatedUserLogFilter(), SecurityContextPersistenceFilter.class);
   }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.config.security;

import java.io.IOException;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import BANK.domain.employee.bo.IEmployeeBO;
import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;

/**
 *
 * @author Mateusz
 */
@Component
public class CustomAuthenticationHandler implements AuthenticationSuccessHandler {
   
    @Autowired
    private IEmployeeSnapshotFinder employeeSnapshotFinder;
    @Autowired
    private IEmployeeBO employeeBO;
    
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();
    
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
//       System.out.println(authentication.getCredentials().toString());
       redirectStrategy.sendRedirect(request, response, "/");
       
//       EmployeeSnapshot emp = employeeSnapshotFinder.findByUsername(authentication.getName());
//      String uuid = UUID.randomUUID().toString();
//      employeeBO.newPassword(emp.getId(), uuid);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.config.security;

import java.util.UUID;
import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import BANK.domain.employee.bo.IEmployeeBO;
import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;

/**
 *
 * @author Mateusz
 */
@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
    @Autowired
    private IEmployeeSnapshotFinder employeeSnapshotFinder;
   
    @Override
    public Authentication authenticate(Authentication authentication) 
      throws AuthenticationException {
        String name = authentication.getName();
        String password = authentication.getCredentials().toString();
        EmployeeSnapshot emp = employeeSnapshotFinder.findOneByUsernameAndPassword(name,password); 
        if (emp != null) {
           if( emp.isEnabled())
              return new UsernamePasswordAuthenticationToken(name, password,new ArrayList());
           else 
              return null;
        } else {
            return null;
        }
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}

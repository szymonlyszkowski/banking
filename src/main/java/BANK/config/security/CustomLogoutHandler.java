/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.config.security;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import BANK.domain.employee.bo.IEmployeeBO;
import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;

/**
 *
 * @author Mateusz
 */
@Component
public class CustomLogoutHandler implements LogoutHandler {
   
    @Autowired
    private IEmployeeSnapshotFinder employeeSnapshotFinder;
    @Autowired
    private IEmployeeBO employeeBO;
      
    @Override
    public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
//      EmployeeSnapshot emp = employeeSnapshotFinder.findOneByUsernameAndPassword(authentication.getName(),authentication.getCredentials().toString());
//      employeeBO.block(emp.getId());
    }
}

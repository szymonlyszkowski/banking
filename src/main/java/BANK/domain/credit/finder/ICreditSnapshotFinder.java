package BANK.domain.credit.finder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import BANK.domain.credit.dto.CreditSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ICreditSnapshotFinder {

   CreditSnapshot findById(Long id);

   List<CreditSnapshot> findAll();

   List<CreditSnapshot> findAll(Set<Long> ids);

   List<CreditSnapshot> findActive();
   
   Map<Long, CreditSnapshot> findAllAsMap(Set<Long> ids);

}

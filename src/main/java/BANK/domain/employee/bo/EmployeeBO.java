package BANK.domain.employee.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;

import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.entity.Employee;
import BANK.domain.employee.exception.EmployeeNotExistsException;
import BANK.domain.employee.repo.IEmployeeRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class EmployeeBO
   implements IEmployeeBO {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeBO.class);
   private final IEmployeeRepository employeeRepository;
   private final AuthenticationManagerBuilder auth;
   
   @Autowired
   public EmployeeBO(IEmployeeRepository employeeRepository, AuthenticationManagerBuilder auth) {
      this.employeeRepository = employeeRepository;
      this.auth = auth;
   }

   @Override
   public EmployeeSnapshot add(UUID guid, String name, String lastname, String title, String username,
      LocalDateTime createdAt, String mail,String role,String password) {
      Employee employee = new Employee(guid, title, name, lastname, username, createdAt, mail,role,password);

      employee = employeeRepository.save(employee);
      
      EmployeeSnapshot employeeSnapshot = employee.toSnapshot();

      LOGGER.info("Add Employee <{}> <{}> <{} {}> <{}> <{}> <{}>",
         employeeSnapshot.getId(),
         employeeSnapshot.getGuid(), employeeSnapshot.getLastname(), employeeSnapshot.getFirstname(),
         employeeSnapshot.getTitle(), employeeSnapshot.getUsername(), employeeSnapshot.getMail());

      return employeeSnapshot;
   }

   @Override
   public EmployeeSnapshot edit(long id, UUID guid, String name, String lastname, String title, String username,
      String mail) {

      Employee employee = employeeRepository.findOne(id);

      employee.editEmployee(guid, name, lastname, title, username, mail);

      employeeRepository.save(employee);

      EmployeeSnapshot employeeSnapshot = employee.toSnapshot();

      LOGGER.info("Edit Employee <{}> <{}> <{}> <{}> <{}> <{}>",
         employeeSnapshot.getId(), employeeSnapshot.getLastname(), employeeSnapshot.getFirstname(),
         employeeSnapshot.getTitle(), employeeSnapshot.getUsername(), employeeSnapshot.getMail());
      return employeeSnapshot;
   }

   @Override
   public void block(Long employeeId) {
      Employee employee = employeeRepository.findOne(employeeId);
      employee.block();
      employeeRepository.save(employee);

      EmployeeSnapshot employeeSnapshot = employee.toSnapshot();
      
      LOGGER.info("Employee <{}> <{}> marked as blocked",
         employeeId, employeeSnapshot.getUsername());
   }
   
   @Override
   public void newPassword(Long employeeId,String password) {
      Employee employee = employeeRepository.findOne(employeeId);
      employee.newPassword(password);
      employeeRepository.save(employee);

      EmployeeSnapshot employeeSnapshot = employee.toSnapshot();
      
      LOGGER.info("Employee <{}> <{}> change password",
         employeeId, employeeSnapshot.getUsername());
   }

   @Override
   public EmployeeSnapshot unlock(Long employeeId) {
      Employee employee = employeeRepository.findOne(employeeId);
      employee.unlock();
      employee = employeeRepository.save(employee);

      LOGGER.info("Employee <{}> signed as employeed", employeeId);

      return employee.toSnapshot();
   }

   @Override
   public void delete(Long employeeId) {
      Employee employee = employeeRepository.findOne(employeeId);
      employeeRepository.delete(employee);

      LOGGER.info("Employee <{}> marked as deleted", employeeId);
   }
}

package BANK.domain.employee.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.employee.dto.EmployeeSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IEmployeeBO {

   EmployeeSnapshot add(UUID guid, String firstname, String lastname,
      String title, String username,
      LocalDateTime createdAt, String mail, String role, String password);

   void block(Long employeeId);

   EmployeeSnapshot edit(long id, UUID guid, String name, String lastname,
      String title, String username, String mail);

   EmployeeSnapshot unlock(Long employeeId);

   void delete(Long employeeId);
   
   void newPassword(Long employeeId,String password);

}

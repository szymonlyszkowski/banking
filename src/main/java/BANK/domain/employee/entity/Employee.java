package BANK.domain.employee.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Employee
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotNull
   @Column(unique = true,
      updatable = false)
   private UUID guid;

   @NotEmpty
   private String title;

   @NotEmpty
   private String firstname;

   @NotEmpty
   private String lastname;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;

   @Version
   private long version;

   @NotEmpty
   private String username;

   @NotEmpty
   private String mail;
   
   @NotEmpty
   private String role;
   
   @NotEmpty
   private String password;

   private boolean enabled;

   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime blockedAt;

   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime updatedAt;


   protected Employee() {
   }

   public Employee(UUID guid, String title, String name, String lastname, String username,
      LocalDateTime createdAt, String mail,String role,String password) {
      this.guid = guid;
      this.title = title;
      this.firstname = name;
      this.lastname = lastname;
      this.username = username;
      this.createdAt = createdAt;
      this.mail = mail;
      this.role = role ; 
      this.password = password;
      this.enabled = true;
   }

   public void block() {
      this.enabled = false;
      this.blockedAt = LocalDateTime.now();
   }

   public EmployeeSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }

      return new EmployeeSnapshot(id, guid, title, firstname, lastname, username,
         createdAt, mail, enabled, blockedAt, updatedAt, version,role,password);
   }

   public void editEmployee(UUID guid, String name, String lastname, String title, String username, String mail) {
      this.guid = guid;
      this.firstname = name;
      this.lastname = lastname;
      this.title = title;
      this.username = username;
      this.mail = mail;
      this.updatedAt = LocalDateTime.now();
   }

   public void unlock() {
      this.enabled = true;
      this.blockedAt = null;
   }
   
   public void newPassword(String password){
      this.password = password;
   }

   Long getId() {
      return id;
   }
}

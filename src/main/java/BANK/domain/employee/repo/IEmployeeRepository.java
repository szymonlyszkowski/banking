package BANK.domain.employee.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import BANK.domain.employee.entity.Employee;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface IEmployeeRepository
   extends JpaRepository<Employee, Long> {

   List<Employee> findByUsernameIgnoreCase(String username);

   List<Employee> findByGuid(UUID guid);

   List<Employee> findByEnabledTrue();
   
   Employee findOneByUsernameAndPassword(String username,String password);


}

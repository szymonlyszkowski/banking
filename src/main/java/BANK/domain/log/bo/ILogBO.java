package BANK.domain.log.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import BANK.domain.log.dto.LogSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ILogBO {

   LogSnapshot add( String whoUser, String whonUser,
      String actionName );


}

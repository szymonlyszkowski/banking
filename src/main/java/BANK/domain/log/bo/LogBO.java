package BANK.domain.log.bo;

import java.time.LocalDateTime;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import BANK.domain.log.dto.LogSnapshot;
import BANK.domain.employee.entity.Employee;
import BANK.domain.employee.exception.EmployeeNotExistsException;
import BANK.domain.employee.repo.IEmployeeRepository;
import BANK.domain.log.entity.Log;
import BANK.domain.log.repo.ILogRepository;
import BANK.sharedkernel.annotations.BussinesObject;

/**
 *
 * @author Mateusz.Glabicki
 */
@BussinesObject
public class LogBO
   implements ILogBO {

   private static final Logger LOGGER = LoggerFactory.getLogger(LogBO.class);
   private final ILogRepository logRepository;

   @Autowired
   public LogBO(ILogRepository logRepository) {
      this.logRepository = logRepository;
   }

   @Override
   public  LogSnapshot add( String whoUser, String whonUser,  String actionName ) {
      Log log = new Log(whoUser,whonUser,actionName);

      log = logRepository.save(log);

      LogSnapshot logSnapshot = log.toSnapshot();

      LOGGER.info("Add log <{}> ",
         logSnapshot.getId());

      return logSnapshot;
   }

}

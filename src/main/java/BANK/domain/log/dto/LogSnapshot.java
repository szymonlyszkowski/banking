package BANK.domain.log.dto;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 *
 * @author Mateusz.Glabicki
 */
public class LogSnapshot {

   private final Long id;

   private final String whoUser;
   
   private final String whomUser;
   
   private final String actionName;

   private final LocalDateTime createdAt;

   public LogSnapshot(Long id, String whoUser, String whomUser, String actionName, LocalDateTime createdAt) {
      this.id = id;
      this.whoUser = whoUser;
      this.whomUser = whomUser;
      this.actionName = actionName;
      this.createdAt = createdAt;
   }

   public Long getId() {
      return id;
   }

   public String getWhoUser() {
      return whoUser;
   }

   public String getWhomUser() {
      return whomUser;
   }

   public String getActionName() {
      return actionName;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }




}

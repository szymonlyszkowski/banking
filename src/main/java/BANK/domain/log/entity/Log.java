package BANK.domain.log.entity;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import BANK.config.persistance.converter.LocalDateTimePersistenceConverter;
import BANK.domain.log.dto.LogSnapshot;
import BANK.sharedkernel.exception.EntityInStateNewException;

/**
 *
 * @author Mateusz.Glabicki
 */
@Entity
public class Log
   implements Serializable {

   private static final long serialVersionUID = 7972265007575707207L;

   @Id
   @GeneratedValue(strategy = GenerationType.AUTO)
   private Long id;

   @NotEmpty
   private String whoUser;

   @NotEmpty
   private String whomUser;

   @NotEmpty
   private String actionName;

   @NotNull
   @Convert(converter = LocalDateTimePersistenceConverter.class)
   private LocalDateTime createdAt;


   protected Log() {
   }

   public Log(String whoUser, String whomUser, String actionName) {
      this.whoUser = whoUser;
      this.whomUser = whomUser;
      this.actionName = actionName;
      this.createdAt = LocalDateTime.now();
   }

   public LogSnapshot toSnapshot() {
      if (id == null) {
         throw new EntityInStateNewException();
      }

      return new LogSnapshot(id, whoUser,whomUser, actionName ,createdAt);
   }

}

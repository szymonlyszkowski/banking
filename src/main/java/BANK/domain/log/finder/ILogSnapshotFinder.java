package BANK.domain.log.finder;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.log.dto.LogSnapshot;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ILogSnapshotFinder {

   LogSnapshot findById(Long id);

   List<LogSnapshot> findAll();


   

}

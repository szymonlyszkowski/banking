package BANK.domain.log.finder;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.entity.Employee;
import BANK.domain.employee.repo.IEmployeeRepository;
import BANK.domain.log.dto.LogSnapshot;
import BANK.domain.log.entity.Log;
import BANK.domain.log.repo.ILogRepository;
import BANK.sharedkernel.annotations.Finder;

/**
 *
 * @author Mateusz.Glabicki
 */
@Finder
public class LogSnapshotFinder
   implements ILogSnapshotFinder {

   private final ILogRepository logRepository;

   @Autowired
   public LogSnapshotFinder(ILogRepository logRepository) {
      this.logRepository = logRepository;
   }

   private List<LogSnapshot> convert(List<Log> logs) {
      return logs.stream()
         .map(Log::toSnapshot)
         .collect(Collectors.toList());
   }

   @Override
   public LogSnapshot findById(Long id) {
      Log log = logRepository.findOne(id);
      return log == null ? null : log.toSnapshot();
   }

   @Override
   public List<LogSnapshot> findAll() {
      List<Log> logs = logRepository.findAll();

      return convert(logs);
   }


}

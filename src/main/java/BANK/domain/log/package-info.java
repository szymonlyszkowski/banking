/**
 * This domain is concentrated on log management. It allows to access log data through REST API and manage
 * them in the database.
 */
package BANK.domain.log;

package BANK.domain.log.repo;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import BANK.domain.log.entity.Log;

/**
 *
 * @author Mateusz.Glabicki
 */
public interface ILogRepository
   extends JpaRepository<Log, Long> {


}

package BANK.sharedkernel.constant;

public final class Encoding {

   public static final String DEFAULT_ENCODING = "UTF-8";

   private Encoding() {
   }

}

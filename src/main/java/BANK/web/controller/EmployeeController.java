package BANK.web.controller;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EmployeeController {

  @RequestMapping("/employee/list")
   public String list() {
      return "employee/list";
   }

   @RequestMapping("/employee/common")
   public String common() {
      return "employee/common";
   }

   @RequestMapping("/employee/details")
   public String detail() {
      return "employee/details";
   }
   
   @RequestMapping("/employee/add")
   public String add() {
      return "employee/add";
   }
   
   @RequestMapping("/employee/edit")
   public String edit() {
      return "employee/edit";
   }

}

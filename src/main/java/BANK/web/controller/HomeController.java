package BANK.web.controller;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import BANK.domain.employee.bo.IEmployeeBO;
import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;
import BANK.sharedkernel.constant.Profiles;

@Controller
public class HomeController {

   private final Environment environment;
   private final IEmployeeSnapshotFinder employeeSnapshotFinder;
   private final IEmployeeBO employeeBO;

   @Autowired
   public HomeController(Environment environment,
      IEmployeeSnapshotFinder employeeSnapshotFinder,
      IEmployeeBO employeeBO) {
      this.environment = environment;
      this.employeeSnapshotFinder = employeeSnapshotFinder;
      this.employeeBO = employeeBO ; 
   }

   @RequestMapping("/")
   public ModelAndView root(ModelAndView modelAndView, Principal principal) {
      modelAndView.setViewName("index");

      List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());

      addIfContains(modelAndView, activeProfiles, Profiles.BASIC_AUTHENTICATION);
      addIfContains(modelAndView, activeProfiles, Profiles.DEVELOPMENT);

      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findByUsername(principal.getName());
      modelAndView.addObject("GUID", employeeSnapshot.getGuid().toString());

      return modelAndView;
   }

//   @RequestMapping(value = "/logout",
//      method = RequestMethod.GET)
//   public String logout(Principal principal) {
//      EmployeeSnapshot emp = employeeSnapshotFinder.findByUsername(principal.getName());
//      String uuid = UUID.randomUUID().toString();
//      employeeBO.newPassword(emp.getId(), uuid);
//
//     
//      return "/logout";
//   }
   
   @RequestMapping("/login")
   public ModelAndView login(ModelAndView modelAndView) {
      modelAndView.setViewName("login");

      List<String> activeProfiles = Arrays.asList(environment.getActiveProfiles());

      addIfContains(modelAndView, activeProfiles, Profiles.BASIC_AUTHENTICATION);
      addIfContains(modelAndView, activeProfiles, Profiles.DEVELOPMENT);

      return modelAndView;
   }

   @RequestMapping(value = "/password",
      method = RequestMethod.GET)
   public ModelAndView password(ModelAndView modelAndView) {
      modelAndView.setViewName("password");
      return modelAndView;
   }
   
      @RequestMapping(value = "/password",
      method = RequestMethod.POST)
   public ModelAndView passwordP(ModelAndView modelAndView,@RequestParam("username") String username) {
      modelAndView.setViewName("password");
      EmployeeSnapshot emp = employeeSnapshotFinder.findByUsername(username);
      if (!(emp == null)) {
         String uuid = UUID.randomUUID().toString();
         employeeBO.newPassword(emp.getId(), uuid);
         modelAndView.addObject("yourPassword", "Twoje nowe hasło to: \r\n " + uuid);
      }
      
      return modelAndView;
   }
   
   private void addIfContains(ModelAndView modelAndView, List<String> profiles, String profile) {
      if (profiles.contains(profile)) {
         modelAndView.addObject(profile, true);
      }
   }

}

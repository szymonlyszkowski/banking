package BANK.web.restapi.credit;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.credit.dto.CreditSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class Credit
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long creditId;

   private final LocalDateTime createdAt;
   
   private final LocalDateTime startDate;
   
   private final LocalDateTime finishDate;

   private final Long provison;

   private final Long installment;
   
   private final Double rateOfInterest;
   
   private final Double interest;
   
   private final boolean active;
   
   private final boolean insuranace;
   
   public Credit(CreditSnapshot creditSnapshot) {
      this.creditId = creditSnapshot.getId();
      this.createdAt = creditSnapshot.getCreatedAt();
      this.startDate = creditSnapshot.getStartDate();
      this.finishDate = creditSnapshot.getFinishDate();
      this.provison = creditSnapshot.getProvison();
      this.installment = creditSnapshot.getInstallment();
      this.rateOfInterest = creditSnapshot.getRateOfInterest();
      this.interest = creditSnapshot.getInterest();
      this.active = creditSnapshot.isActive();
      this.insuranace = creditSnapshot.isInsuranace();
   }
   
   public Long getCreditId() {
      return creditId;
   }

   public LocalDateTime getCreatedAt() {
      return createdAt;
   }

   public LocalDateTime getStartDate() {
      return startDate;
   }

   public LocalDateTime getFinishDate() {
      return finishDate;
   }

   public Long getProvison() {
      return provison;
   }

   public Long getInstallment() {
      return installment;
   }

   public Double getRateOfInterest() {
      return rateOfInterest;
   }

   public Double getInterest() {
      return interest;
   }

   public boolean isActive() {
      return active;
   }

   public boolean isInsuranace() {
      return insuranace;
   }


  
}

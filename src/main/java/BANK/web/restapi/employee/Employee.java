package BANK.web.restapi.employee;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

import org.springframework.hateoas.ResourceSupport;

import BANK.domain.employee.dto.EmployeeSnapshot;

import com.fasterxml.jackson.annotation.JsonView;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 *
 * @author Mateusz.Glabicki
 */

public class Employee
   extends ResourceSupport
   implements Serializable {

   private static final long serialVersionUID = 123364444898230931L;

   private final Long employeeId;

   private final UUID guid;

   private final String title;

   private final String firstname;

   private final String lastname;

   private final String username;

   private final LocalDateTime createdAt;

   private final String mail;

   private final boolean enabled;

   private final LocalDateTime blockedAt;

   private final LocalDateTime updatedAt;

   private final long updatesCount;


   public Employee(EmployeeSnapshot employeeSnapshot) {
      this.employeeId = employeeSnapshot.getId();
      this.guid = employeeSnapshot.getGuid();
      this.title = employeeSnapshot.getTitle();
      this.firstname = employeeSnapshot.getFirstname();
      this.lastname = employeeSnapshot.getLastname();
      this.username = employeeSnapshot.getUsername();
      this.createdAt = employeeSnapshot.getCreatedAt();
      this.mail = employeeSnapshot.getMail();
      this.enabled = employeeSnapshot.isEnabled();
      this.blockedAt = employeeSnapshot.getBlockedAt();
      this.updatedAt = employeeSnapshot.getUpdatedAt();
      this.updatesCount = employeeSnapshot.getVersion();
   }
   


   public Long getEmployeeId() {
      return employeeId;
   }


   public UUID getGuid() {
      return guid;
   }


   public String getTitle() {
      return title;
   }


   public String getFirstname() {
      return firstname;
   }


   public String getLastname() {
      return lastname;
   }


   public String getUsername() {
      return username;
   }


   public LocalDateTime getCreatedAt() {
      return createdAt;
   }


   public String getMail() {
      return mail;
   }


   public boolean isEnabled() {
      return enabled;
   }


   public LocalDateTime getBlockedAt() {
      return blockedAt;
   }


   public LocalDateTime getUpdatedAt() {
      return updatedAt;
   }


   public long getUpdatesCount() {
      return updatesCount;
   }

   
}

package BANK.web.restapi.employee;

import java.security.Principal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.employee.bo.IEmployeeBO;

@RestController
@RequestMapping("/api/employee")
public class EmployeeApi {

   
   private final IEmployeeSnapshotFinder employeeSnapshotFinder;

   private final IEmployeeBO employeeBO;

   @Autowired
   public EmployeeApi(IEmployeeSnapshotFinder employeeSnapshotFinder, IEmployeeBO employeeBO) {
      this.employeeSnapshotFinder = employeeSnapshotFinder;
      this.employeeBO = employeeBO;
   }


   @RequestMapping(method = RequestMethod.GET)
   public List<Employee> list() {
      List<EmployeeSnapshot> employeeSnapshots = employeeSnapshotFinder.findAll();

      return employeeSnapshots.stream()
         .map(Employee::new)
         .collect(Collectors.toList());
   }


   @RequestMapping(value = "/active",
      method = RequestMethod.GET)
   public List<Employee> active() {
      List<EmployeeSnapshot> employeeSnapshots = employeeSnapshotFinder.findActive();

      return employeeSnapshots.stream()
         .map(Employee::new)
         .collect(Collectors.toList());
   }


   public HttpEntity<Employee> get(@PathVariable("id") long id) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findById(id);

      if (employeeSnapshot == null) {
         return new ResponseEntity<>(HttpStatus.NOT_FOUND);
      } else {
         return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
      }
   }

   @RequestMapping(value = "/logged",
      method = RequestMethod.GET)
   public Employee getLoggedEmployee(Principal principal) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findByUsername(principal.getName());

      return new Employee(employeeSnapshot);

   }


   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Employee> add(@RequestBody EmployeeNew employeeNew) {

      EmployeeSnapshot employeeSnapshot = employeeBO.add(UUID.randomUUID(), employeeNew.getFirstname(),
         employeeNew.getLastname(),
         "User", employeeNew.getFirstname() + '.' + employeeNew.getLastname(),
         LocalDateTime.now(), employeeNew.getFirstname() + "." + employeeNew.getLastname() + "@nsa.pl", "ROLE_USER",
         "password");

      return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(method = RequestMethod.PUT,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity<Employee> update(
      @RequestBody EmployeeEdit employeeEdit) {

      EmployeeSnapshot foundedEmployee = employeeSnapshotFinder.findById(employeeEdit.getEmployeeId());

      EmployeeSnapshot employeeSnapshot = employeeBO.edit(employeeEdit.getEmployeeId(), foundedEmployee.getGuid(),
         employeeEdit.getFirstname(), employeeEdit.getLastname(),
         "User", employeeEdit.getFirstname() + '.' + employeeEdit.getLastname(),
         employeeEdit.getFirstname() + "." + employeeEdit.getLastname() + "@nsa.pl");

      return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "block/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity<Employee> block(@PathVariable("id") Long employeeId) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findById(
         employeeId);
      if (employeeId != null && !employeeSnapshot.getRole().equals("ROLE_ADMIN")) {
         employeeBO.block(employeeId);
         return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
      }

      return new ResponseEntity<>(HttpStatus.FORBIDDEN);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "unlock/{id}",
      method = RequestMethod.PUT)
   public HttpEntity<Employee> unlock(@PathVariable("id") Long employeeId) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findById(
         employeeId);
      if (employeeId != null) {
         employeeBO.unlock(employeeId);
      }

      return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
   }


   @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
   @RequestMapping(value = "delete/{id}",
      method = RequestMethod.DELETE)
   public HttpEntity delete(@PathVariable("id") Long employeeId) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findById(
         employeeId);
      if (employeeId != null) {
         employeeBO.delete(employeeId);
      }

      return new ResponseEntity<>(new Employee(employeeSnapshot), HttpStatus.OK);
   }


   @RequestMapping(value = "/external/block/{id}",
      method = RequestMethod.PUT)
   public HttpEntity extBlock(@PathVariable("id") Long employeeId) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findById(
         employeeId);

      if (employeeSnapshot != null) {
         employeeBO.unlock(employeeId);
         return new ResponseEntity(HttpStatus.OK);
      }

      return new ResponseEntity(HttpStatus.BAD_REQUEST);
   }

}

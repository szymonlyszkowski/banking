/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.employee;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;



/**
 *
 * @author Mateusz
 */
public class EmployeeEdit {
   
   @NotNull
   private Long employeeId;
   
   @NotEmpty
   private String firstname;
   
   @NotEmpty
   private String lastname;

   public Long getEmployeeId() {
      return employeeId;
   }

   public void setEmployeeId(Long employeeId) {
      this.employeeId = employeeId;
   }

   public String getFirstname() {
      return firstname;
   }

   public void setFirstname(String firstname) {
      this.firstname = firstname;
   }

   public String getLastname() {
      return lastname;
   }

   public void setLastname(String lastname) {
      this.lastname = lastname;
   }
      
   
}

package BANK.web.restapi.log;

import java.security.Principal;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import BANK.domain.employee.dto.EmployeeSnapshot;
import BANK.domain.employee.finder.IEmployeeSnapshotFinder;
import BANK.web.restapi.employee.Employee;

import com.fasterxml.jackson.annotation.JsonView;

import BANK.domain.employee.finder.EmployeeSnapshotFinder;
import BANK.domain.log.bo.ILogBO;

@RestController
@RequestMapping("/api/log")
public class LogApi {

   private final ILogBO logBO;
   private final IEmployeeSnapshotFinder employeeSnapshotFinder;

   @Autowired
   public LogApi(ILogBO logBO,IEmployeeSnapshotFinder employeeSnapshotFinder) {
      this.logBO = logBO;
      this.employeeSnapshotFinder = employeeSnapshotFinder;
   }

   @RequestMapping(method = RequestMethod.POST,
      consumes = MediaType.APPLICATION_JSON_VALUE)
   public HttpEntity getLoggedEmployee(Principal principal, @RequestBody LogNew logNew) {
      EmployeeSnapshot employeeSnapshot = employeeSnapshotFinder.findByUsername(principal.getName());
      
      logBO.add(employeeSnapshot.getUsername(), logNew.getWhonUser(), logNew.getActionName());
      
      return new ResponseEntity(HttpStatus.OK);
      
   }
   
   
}

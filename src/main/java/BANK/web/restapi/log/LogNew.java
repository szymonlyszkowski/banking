/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BANK.web.restapi.log;

import org.hibernate.validator.constraints.NotEmpty;

/**
 *
 * @author Mateusz
 */
public class LogNew {
   
   @NotEmpty
   private String whonUser;
   
   @NotEmpty
   private String actionName;

   public String getWhonUser() {
      return whonUser;
   }

   public void setWhonUser(String whonUser) {
      this.whonUser = whonUser;
   }

   public String getActionName() {
      return actionName;
   }

   public void setActionName(String actionName) {
      this.actionName = actionName;
   }
   
   

}

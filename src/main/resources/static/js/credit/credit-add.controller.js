(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditAddCtrl', CreditAddCtrl);

   CreditAddCtrl.$inject = [
      '$scope',
      '$state',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'languageUtil'];

   function CreditAddCtrl($scope, $state, Credit, CommonUtilService, logToServerUti, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);


      $scope.credit = {
         rateOfInterest: '',
         interest: '',
         insuranace: false,
         installment :'',
         provison : '',
         finishDate: '',
         startDate:''
      };
      
      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.rateOfInterest = $scope.credit.rateOfInterest;
         credit.interest = $scope.credit.interest;
         credit.insuranace = $scope.credit.insuranace;
         credit.installment = $scope.credit.installment;
         credit.provison = $scope.credit.provison;
         credit.finishDate = $scope.credit.finishDate;
         credit.startDate = $scope.credit.startDate;

         credit.$save()
                 .then(function (result) {
                    $state.go('credit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();
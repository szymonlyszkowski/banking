(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditDetailsCtrl', CreditDetailsCtrl);

   CreditDetailsCtrl.$inject = ['$scope', 'credit'];

   function CreditDetailsCtrl($scope, credit) {
      $scope.credit = credit;
      $scope.$parent.changeView('DETAILS');
   }
})();

(function () {
   'use strict';

   angular
           .module('bank')
           .controller('CreditEditCtrl', CreditEditCtrl);

   CreditEditCtrl.$inject = [
      '$scope',
      '$state',
      'credit',
      'Credit',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function CreditEditCtrl($scope, $state,credit, Credit, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.credit = {
         creditId :credit.creditId,
         rateOfInterest : credit.rateOfInterest,
         interest : credit.interest,
         insuranace : credit.insuranace,
         installment : credit.installment,
         provison : credit.provison,
         finishDate : credit.finishDate,
         startDate : credit.startDate
      };
      $scope.saveCredit = saveCredit;

      function saveCredit() {
         var credit = new Credit();
         credit.rateOfInterest = $scope.credit.rateOfInterest;
         credit.interest = $scope.credit.interest;
         credit.insuranace = $scope.credit.insuranace;
         credit.installment = $scope.credit.installment;
         credit.provison = $scope.credit.provison;
         credit.finishDate = $scope.credit.finishDate;
         credit.startDate = $scope.credit.startDate;
         credit.creditId = $scope.credit.creditId;   
         credit.$update()
                 .then(function (result) {
                    $state.go('credit.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();
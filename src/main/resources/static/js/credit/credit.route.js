(function () {
   'use strict';

   angular
           .module('bank')
           .config(creditProvider);

   creditProvider.$inject = ['$stateProvider', '$breadcrumbProvider'];

   function creditProvider($stateProvider, $breadcrumbProvider) {
      var resolveCredits = ['$state', 'Credit', 'popupService', 'logToServerUtil', loadCredits];
      var resolveSingleCredit = ['$state', '$stateParams', 'Credit', 'popupService', 'logToServerUtil',
         loadSingleCredit];

      $stateProvider
              .state('credit', {
                 parent: 'root',
                 url: '/credit',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('credit.list', {
                 url: '/list',
                 reloadOnSearch: false,
                 templateUrl: '/credit/list',
                 controller: 'CreditListCtrl',
                 resolve: {
//                    credits: resolveCredits,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('credit/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('credit.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/credit/add',
                 controller: 'CreditAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('credit/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('credit.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/credit/edit',
                 controller: 'CreditEditCtrl',
                 resolve: {
                    credit: resolveSingleCredit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('credit/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('credit.common', {
                 url: '/{id:int}',
                 templateUrl: '/credit/common',
                 controller: 'CreditCommonCtrl',
                 redirectTo: 'credit.common.details',
                 resolve: {
                    credit: resolveSingleCredit,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('credit/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('credit.common.details', {
                 url: '/details',
                 templateUrl: '/credit/details',
                 controller: 'CreditDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('credit/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadCredits($state, Credit, popupService, logToServerUtil) {
         var creditsPromise = Credit.query().$promise;
         console.log('dziala');
         creditsPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Credits failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return creditsPromise;
      }

      function loadSingleCredit($state, $stateParams, Credit, popupService, logToServerUtil) {
         var creditPromise = Credit.get({id: $stateParams.id}).$promise;
         creditPromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Credit failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return creditPromise;
      }
   }
})();

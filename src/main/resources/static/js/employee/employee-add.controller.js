(function () {
   'use strict';

   angular
           .module('bank')
           .controller('EmployeeAddCtrl', EmployeeAddCtrl);

   EmployeeAddCtrl.$inject = [
      '$scope',
      '$state',
      'Employee',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function EmployeeAddCtrl($scope, $state, Employee, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.employee = {
         firstname: '',
         lastname: ''
      };
      $scope.saveEmployee = saveEmployee;

      function saveEmployee() {
         var employee = new Employee();
         employee.firstname = $scope.employee.firstname;
         employee.lastname = $scope.employee.lastname;

         employee.$save()
                 .then(function (result) {
                    $state.go('employee.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();
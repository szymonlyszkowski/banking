(function () {
   'use strict';

   angular
           .module('bank')
           .controller('EmployeeCommonCtrl', EmployeeCommonCtrl);

   EmployeeCommonCtrl.$inject = ['$scope', 'employee'];

   function EmployeeCommonCtrl($scope, employee) {
      $scope.employee = employee;
      $scope.changeView = changeView;
      function changeView(state) {
         $scope.view = {
            details: false
         };
         
         switch (state) {
            case 'DETAILS':
               $scope.view.details = true;
               break;
            default:
               break;
         }
      }
   }
})();

(function () {
   'use strict';

   angular
           .module('bank')
           .controller('EmployeeEditCtrl', EmployeeEditCtrl);

   EmployeeEditCtrl.$inject = [
      '$scope',
      '$state',
      'employee',
      'Employee',
      'CommonUtilService',
      'logToServerUtil',
      'popupService',
      'languageUtil'];

   function EmployeeEditCtrl($scope, $state,employee, Employee, CommonUtilService, logToServerUtil, popupService, languageUtil) {
      CommonUtilService.initCommonAndOperator($scope);

      $scope.employee = {
         firstname: employee.firstname,
         lastname: employee.lastname,
         employeeId :employee.employeeId
      };
      $scope.saveEmployee = saveEmployee;

      function saveEmployee() {
         var employee = new Employee();
         employee.firstname = $scope.employee.firstname;
         employee.lastname = $scope.employee.lastname;
         employee.employeeId = $scope.employee.employeeId;   
         employee.$update()
                 .then(function (result) {
                    $state.go('employee.list');
                 }, function (reason) {
                    logToServerUtil.trace('Save Questionnaire Template failure', reason);
                 });
      }
   }
})();
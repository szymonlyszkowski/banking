(function () {
   'use strict';

   angular
           .module('bank')
           .controller('EmployeeListCtrl', EmployeeListCtrl);

   EmployeeListCtrl.$inject = [
      '$scope',
      'Employee',
      'employees',
      '$stateParams',
      '$location',
      '$timeout'];

   function EmployeeListCtrl($scope, Employee, employees, $stateParams, $location, $timeout) {
      $scope.loading = false;
      $scope.employees = employees;
      $scope.filterRow = true;
      $scope.searchText = [];
      $scope.searchText.enabled = 'false';
      $scope.title = 'breadcrumb.employee';

      var firstnameChangeTimeout;
      var lastnameChangeTimeout;

      if ($stateParams.enabled !== undefined) {
         $scope.searchText.enabled = $stateParams.enabled;
      }

      $scope.searchText.firstname = $stateParams.firstname;
      $scope.searchText.lastname = $stateParams.lastname;
      $scope.searchText.title = $stateParams.title;
      $scope.refresh = refresh;

      $scope.updateTitleParam = updateTitleParam;
      $scope.updateStatusParam = updateStatusParam;
      $scope.updateFirstnameParam = updateFirstnameParam;
      $scope.updateLastnameParam = updateLastnameParam;
      $scope.logAction = logAction;
      $scope.blockUser = blockUser;
      $scope.unblockUser = unblockUser;
      $scope.deleteUser = deleteUser;

      var employeeTitles = [];
      employeeTitles.sort();
      $scope.employeeTitles = employeeTitles;

      function reloadEmployees() {
         $scope.loading = true;
         Employee.query().$promise
                 .then(function (data) {
                    $scope.employees = data;
                    $scope.loading = false;
                 });
      }

      function logAction(whonUser, actionName) {
         $scope.loading = true;
         var action = new Employee();
         action.whonUser = whonUser;
         action.actionName = actionName;
         action.$logAction()
                 .then(function (data) {
                    $scope.loading = false;
                 });
      }

      function blockUser(employeeId) {
         $scope.loading = true;
         Employee.block({id: employeeId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.employees.length; i++) {
                       if ($scope.employees[i].employeeId === employeeId) {
                          $scope.employees[i].enabled = false;
                       }
                    }
                 });
      }

      function deleteUser(employeeId) {
         $scope.loading = true;
         Employee.delete({id: employeeId}).$promise
                 .then(function (data) {
                    $scope.loading = false;
                    for (var i = 0; i < $scope.employees.length; i++) {
                       if ($scope.employees[i].employeeId === employeeId) {
                          $scope.employees.splice(i, 1);
                       }
                    }
                 });
      }

      function unblockUser(employeeId) {
         $scope.loading = true;
         Employee.unblock({id: employeeId}).$promise
                 .then(function (data) {
                    for (var i = 0; i < $scope.employees.length; i++) {
                       if ($scope.employees[i].employeeId === employeeId) {
                          $scope.employees[i].enabled = true;
                       }
                    }
                    $scope.loading = false;
                 });
      }

      function refresh() {
         Employee.refresh().$promise
                 .then(function () {
                    reloadEmployees();
                    $('#ProcessingModal').modal('hide');
                 });
      }

      function updateTitleParam() {
         $stateParams.title = $scope.searchText.title;
         $location.search($stateParams);
      }
      function updateStatusParam() {
         $stateParams.enabled = $scope.searchText.enabled;
         $location.search($stateParams);
      }
      function updateFirstnameParam() {
         if (firstnameChangeTimeout !== undefined) {
            $timeout.cancel(firstnameChangeTimeout);
         }
         firstnameChangeTimeout = $timeout(updateFirstnameParamAtTimeout, 1000);
      }
      function updateLastnameParam() {
         if (lastnameChangeTimeout !== undefined) {
            $timeout.cancel(lastnameChangeTimeout);
         }
         lastnameChangeTimeout = $timeout(updateLastnameParamAtTimeout, 1000);
      }

      function updateFirstnameParamAtTimeout() {
         if ($scope.searchText.firstname) {
            $stateParams.firstname = $scope.searchText.firstname;
         } else {
            $stateParams.firstname = undefined;
         }
         $location.search($stateParams);
         firstnameChangeTimeout = undefined;
      }
      function updateLastnameParamAtTimeout() {
         if ($scope.searchText.lastname) {
            $stateParams.lastname = $scope.searchText.lastname;
         } else {
            $stateParams.lastname = undefined;
         }
         $location.search($stateParams);
         lastnameChangeTimeout = undefined;
      }
   }
})();
(function () {
   'use strict';

   angular
           .module('bank')
           .config(employeeProvider);

   employeeProvider.$inject = ['$stateProvider', '$breadcrumbProvider'];

   function employeeProvider($stateProvider, $breadcrumbProvider) {
      var resolveEmployees = ['$state', 'Employee', 'popupService', 'logToServerUtil', loadEmployees];
      var resolveSingleEmployee = ['$state', '$stateParams', 'Employee', 'popupService', 'logToServerUtil',
         loadSingleEmployee];

      $stateProvider
              .state('employee', {
                 parent: 'root',
                 url: '/employee',
                 abstract: true,
                 template: '<ui-view />'
              })
              .state('employee.list', {
                 url: '/list?lastname&firstname&title&enabled',
                 reloadOnSearch: false,
                 templateUrl: '/employee/list',
                 controller: 'EmployeeListCtrl',
                 resolve: {
                    employees: resolveEmployees,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('employee/list');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('employee.add', {
                 url: '/add',
                 reloadOnSearch: false,
                 templateUrl: '/employee/add',
                 controller: 'EmployeeAddCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('employee/add');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('employee.edit', {
                 url: '/edit/{id:int}',
                 reloadOnSearch: false,
                 templateUrl: '/employee/edit',
                 controller: 'EmployeeEditCtrl',
                 resolve: {
                    employee: resolveSingleEmployee,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('employee/edit');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('employee.common', {
                 url: '/{id:int}',
                 templateUrl: '/employee/common',
                 controller: 'EmployeeCommonCtrl',
                 redirectTo: 'employee.common.details',
                 resolve: {
                    employee: resolveSingleEmployee,
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('employee/common');
                          return $translate.refresh();
                       }]
                 }
              })
              .state('employee.common.details', {
                 url: '/details',
                 templateUrl: '/employee/details',
                 controller: 'EmployeeDetailsCtrl',
                 resolve: {
                    translations: ['$translate', '$translatePartialLoader', function ($translate, $translatePartialLoader) {
                          $translatePartialLoader.addPart('employee/details');
                          return $translate.refresh();
                       }]
                 }
              });


      function loadEmployees($state, Employee, popupService, logToServerUtil) {
         var employeesPromise = Employee.query().$promise;
         employeesPromise
                 .then(function () {
                 }, function (reason) {
                    logToServerUtil.trace('get Employees failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return employeesPromise;
      }

      function loadSingleEmployee($state, $stateParams, Employee, popupService, logToServerUtil) {
         var employeePromise = Employee.get({id: $stateParams.id}).$promise;
         employeePromise
                 .catch(function (reason) {
                    logToServerUtil.trace('get Employee failed', reason);
                    popupService.error('shared.popup.general-failure.title',
                            'shared.popup.general-failure.body');
                    $state.go('dashboard');
                 });
         return employeePromise;
      }
   }
})();

(function () {
   'use strict';

   angular
           .module('bank')
           .factory('Employee', Employee);

   Employee.$inject = ['$resource', 'dateResolveUtil'];

   function Employee($resource, dateResolveUtil) {
      return $resource('/api/employee/:id', {}, {
         update: {
            method: 'PUT'
         },
         query: {
            method: 'GET',
            isArray: true,
            transformResponse: transformResponse
         },
         get: {
            method: 'GET',
            transformResponse: transformResponse
         },
         refresh: {
            method: 'POST',
            url: '/api/employee/refresh',
            isArray: false
         },
         logAction: {
            method: 'POST',
            url: '/api/log',
            isArray: false
         },
         block : {
            method:'DELETE',
            url: '/api/employee/block/:id',
            params:{id : '@id'}
         },
         unblock: {
            method:'PUT',
            url: '/api/employee/unlock/:id',
            params:{id : '@id'}
         },
         delete:{
            method:'DELETE',
            url: '/api/employee/delete/:id',
            params:{id : '@id'}
         },
         titles: {
            method: 'GET',
            url: '/api/employee/title',
            isArray: true
         },
         getLoggedEmployee: {
            method: 'GET',
            params: {},
            url: '/api/employee/logged',
            transformResponse: transformResponse
         },
         active: {method: 'GET',
            url: '/api/employee/active',
            isArray: true,
            transformResponse: transformResponse
         }
      });

      ////////////

      function transformResponse(data) {
         data = angular.fromJson(data);

         if ($.isArray(data)) {
            for (var i in data) {
               transformSingle(data[i]);
            }
         } else {
            transformSingle(data);
         }

         return data;
      }

      function transformSingle(employee) {
         employee.createdAt = dateResolveUtil.convertToDate(employee.createdAt);
         employee.updatedAt = dateResolveUtil.convertToDate(employee.updatedAt);
         employee.blockedAt = dateResolveUtil.convertToDate(employee.blockedAt);
      }
   }
})();
